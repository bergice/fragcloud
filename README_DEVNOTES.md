## Links
- https://developers.google.com/api-client-library/java/google-api-java-client/media-upload
- https://developers.google.com/drive/v3/web/manage-uploads
- https://developers.google.com/drive/v3/web/search-parameters
- https://www.tutorialspoint.com/maven/maven_build_automation.htm

## Notes
- We can achieve GoogleDrive file listening by querying every sync: `appParameter = fragcloud and lastModified >= db.file.lastModified`
- To ensure we sync files we only need to sync we can use a custom field in every Google Drive file which contains the `lastModified` date. Then we would know when to 


2. save files to db once created from path
3. save root folder id to db so we wont have to look it up every time
4. upload relative directories
5. profile speed & find bottlenecks