package fragcloud;

import fragcloud.backend.cloud.CloudService;
import fragcloud.backend.database.BaseDatabase;
import fragcloud.backend.sync.SyncService;
import fragcloud.frontend.App;
import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        // Ensure the database is ready before any threads start
        BaseDatabase.getInstance().initiate();

        if (!cli(args)) Application.launch(App.class, args);
    }

    public static boolean cli(String[] args) {
//        if (args.length == 0) return false;

        CloudService.getInstance().addBuckets();
        SyncService syncService = new SyncService();
        syncService.addFileWatchers();

        return true;
    }
}
