package fragcloud.backend.framework;

import fragcloud.backend.database.BaseDatabase;
import fragcloud.utils.Logging;
import fragcloud.utils.Utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BaseEntity {
    // Fields
    @DatabaseColumn
    int id;

    public static String getTableName() {
        Logging.error("Table name for entity not defined!");
        return null;
    }

    private void loadFromArray(Map<String, Object> fields) {
        Class<?> clazz = getClass();
        try {
            for(Map.Entry<String, Object> entry : fields.entrySet()) {
                String key = entry.getKey();
                Object fieldValue = entry.getValue();

                Field field = Utils.getFieldExtended(clazz, key);
                if (field != null
                    && fieldValue != null
                    /*&& field.isAnnotationPresent(DatabaseColumn.class)*/) {
                    field.set(this, fieldValue);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static <T extends BaseEntity> BaseEntity createFromArray(Map<String, Object> fields, Class<T> clazz) {
        try {
            // TODO: create record (use from extended class)
            BaseEntity entity;
            if (fields.containsKey("clazz")) {
                Class myClass = Class.forName(BaseEntity.class.getPackage().getName() + '.' + fields.get("clazz"));
                Constructor constructor = myClass.getConstructor();
                entity = (BaseEntity) constructor.newInstance();
            }
            else {
                entity = (BaseEntity) clazz.newInstance();
            }

            // TODO: loadFromArray(fields);
            entity.loadFromArray(fields);
            return entity;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T extends BaseEntity> List<T> getAllWhere(String query, Class<T> clazz, Object... args) {
        try {
            Method getTableName = clazz.getMethod("getTableName");
            String tableName = (String) getTableName.invoke(null);

            query = String.format(query, args);
            query = String.format("select * from %s where %s", tableName, query);

            // TODO: execute query
            List<Map<String, Object>> items = BaseDatabase.getInstance().getItems(query);

            List<T> entities = new ArrayList<>();
            // TODO: for each resulting item
        //        assert items != null;
            if (items.size() > 0) {
                for(Map<String, Object> item : items) {
                    // TODO: createFromArray(row)
                    T entity = (T)createFromArray(item, clazz);
                    entities.add(entity);
                }
            }
            // TODO: return array(records)
            return entities;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public boolean isNew() {
        return getId() == 0;
    }

    private List<Field> __getDatabaseFields() {
        Class<?> clazz = getClass();
        List<Field> fields = Utils.getFieldsExtended(clazz);
        if (fields != null) {
            for (int i=0; i<fields.size(); i++) {
                Field field = fields.get(i);
                if (field == null || !field.isAnnotationPresent(DatabaseColumn.class) || field.getName().equals("id")) {
                    fields.remove(field);
                    i--;
                }
            }
        }
        return fields;
    }

    private String __mapFieldToSql(Field field) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (    field.getType().isAssignableFrom(Integer.class) ||  field.getType().isAssignableFrom(int.class)
                ||  field.getType().isAssignableFrom(Float.class)   ||  field.getType().isAssignableFrom(float.class)) {
                stringBuilder.append(field.get(this));
            } else if (field.getType().isAssignableFrom(Boolean.class) ||  field.getType().isAssignableFrom(boolean.class)) {
                stringBuilder.append('\'').append(field.get(this)).append('\'');
            } else if (field.getType().isAssignableFrom(String.class)) {
                stringBuilder.append('\'').append(field.get(this)).append('\'');
            } else {
                stringBuilder.append('\'').append(field.get(this)).append('\'');
            }
        } catch (IllegalAccessException e) {
            Logging.warning("Illegal access for field %s", field.getName());
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    // TODO: Refactor to BaseDatabase and override for SqlLiteDatabase, we should only supply the keys/values and the database should format this.
    private String __getSaveQuery() {
        String tableName = Utils.invokeMethodFor(getClass(), "getTableName");

        List<Field> fields = __getDatabaseFields();

        StringBuilder values = new StringBuilder();
        Field field;
        if (isNew()) {
            StringBuilder keys = new StringBuilder();
            for (int i=0; i<fields.size(); i++) {
                field = fields.get(i);
                keys.append(field.getName());
                values.append(__mapFieldToSql(field));
                if (i<fields.size()-1) keys.append(',');
                if (i<fields.size()-1) values.append(',');
            }
            return String.format("INSERT INTO %s (%s) VALUES (%s)", tableName, keys.toString(), values.toString());
        }
        else {
            for (int i=0; i<fields.size(); i++) {
                field = fields.get(i);
                values.append(field.getName()).append("=").append(__mapFieldToSql(field));
                if (i<fields.size()-1) values.append(',');
            }
            return String.format("UPDATE %s SET %s WHERE id = %d", tableName, values.toString(), getId());
        }
    }

    public void save() {

        String query = __getSaveQuery();
        if (isNew()) {
            id = BaseDatabase.getInstance().queryInsert(query);
        }
        else {
            BaseDatabase.getInstance().queryUpdate(query);
        }
    }
}
