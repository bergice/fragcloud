package fragcloud.backend.framework;

public class DirectoryEntity extends BaseEntity {
    // Database Fields
    @DatabaseColumn
    protected String directory;

    @DatabaseColumn
    protected long lastSync;

    public static String getTableName() {
        return "directory";
    }

    public String getDirectory() {
        return directory;
    }
}
