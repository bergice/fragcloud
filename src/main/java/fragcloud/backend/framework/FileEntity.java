package fragcloud.backend.framework;

import fragcloud.backend.cloud.CloudService;
import fragcloud.utils.Utils;
import fragcloud.utils.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

public class FileEntity extends BaseEntity {
    // Database Fields
    @DatabaseColumn
    protected int bucket;

    @DatabaseColumn
    protected int directory;

    @DatabaseColumn
    protected String path;

    @DatabaseColumn
    protected boolean synced;

    @DatabaseColumn
    protected String md5;

    @DatabaseColumn
    protected long lastModified;

    @DatabaseColumn
    protected long size;

    @DatabaseColumn
    protected String name;

    @DatabaseColumn
    protected String mimeType;

    @DatabaseColumn
    protected String remoteId;

    // Other fields
    private Path _path;
    private File _file;

    public static String getTableName() {
        return "file";
    }

    public static void processForPath(Path path, BasicFileAttributes attr) {
        // TODO: Trigger FileChanged to listener. This could be even abstracted more, so we could have the indexing for the buckets as well,
        // TODO:    but different behavior when this is triggered.
        // TODO: So for GoogleCloudBucket we would trigger it to update the local file (on the computer)
        // TODO: And with DirectorySyncer we could trigger it to update the remote file (on the google bucket)

        FileEntity file = FileEntity.getForPath(path); // match against path.name + directory.name
        if (file.isDirty()) { // md5 differs, modified dates do not match, etc..
            file.index();
        }
        // we don't need the synced column, it should really be syncStatus(NONE,ON_REMOTE,ON_LOCAL,ON_BOTH)
        // we just check it against the cloud -- file records also represent the remote/cloud file, so you can look the file up from both ends
        // force flag could check it if the file is on the other end and matches too
        if (!file.isSynced()) {
//            cloud.upload();
            file.sync();
        }
    }

    public static FileEntity getForPath(Path path) {
        List<FileEntity> entities = getAllWhere("path = '%s'", FileEntity.class, path.toFile().getAbsolutePath());
        assert entities.size() <= 1;
        if (entities.size() == 1) {
            return entities.get(0);
        }
        // create the bloody file
        FileEntity file = new FileEntity();
        file.setPath(path);
        return file;
    }

    public boolean isDirty() {
        // TODO: check if md5 doesn't match, modified dates don't match, etc...

        // local file was updated after remote was synced
        long fieldLastModified = getLastModified(false);
        long fileLastModified = getPath().toFile().lastModified();

        if (fieldLastModified <= fileLastModified) {
            return true;
        }

        return false;
    }

    public void index() {
        // TODO: index in the database
        updateFieldsFromFile();
    }

    private void updateFieldsFromFile() {
        try {
            size = Files.size(getPath());
            lastModified = getFile().lastModified();
            md5 = Utils.computeMd5(getFile());
            name = getFile().getName();
            mimeType = Files.probeContentType(getPath());
        } catch (IOException e) {
            // TODO: file invalid?
            e.printStackTrace();
        }
    }

    public boolean isSynced() {
        // TODO: check that the file also exists and matches on remote
        return getRemoteId() != null;
    }

    public void sync() {
        // TODO: upload to remote
        Stopwatch stopwatch = new Stopwatch("Uploading file %s to remote...", getAbsolutePath());
        try {
            CloudService.getInstance().uploadFile(this);
            this.save();
            stopwatch.end("Upload for %s complete - indexed and saved file to database", getAbsolutePath());
        } catch (Exception e) {
            stopwatch.end("Uploading %s failed", getAbsolutePath());
            e.printStackTrace();
        }
    }

    public Path getPath() {
        if (_path == null) _path = Paths.get(path);
        return _path;
    }

    public void setPath(Path path) {
        this.path = path.toFile().getAbsolutePath();

        updateFieldsFromFile();
    }

    public long getSize(boolean checkDirty) {
        if (checkDirty) {
            if (isDirty()) {
                index();
            }
        }
        return size;
    }

    public long getLastModified(boolean checkDirty) {
        if (checkDirty) {
            if (isDirty()) {
                index();
            }
        }
        return lastModified;
    }

    public String getName() {
        return name;
    }

    public String getMimeType() {
        return mimeType;
    }

    public File getFile() {
        Path path = getPath();
        if (_file == null) _file = path.toFile();
        return _file;
    }

    public String getAbsolutePath() {
        return getFile().getAbsolutePath();
    }

    /**
     * Relative to root directory of synced folder
     */
    public String getRelativePath() {
        // TODO:
        return null;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public void setBucket(int id) {
        this.bucket = id;
    }
}
