package fragcloud.backend.framework;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import fragcloud.utils.Logging;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class GoogleDriveBucketEntity extends BucketEntity {
    /** Application name. */
    private static final String APPLICATION_NAME = "fragcloud";

    /** Folder to put files into */
    private static final String GOOGLE_DRIVE_FOLDER_NAME = ".fragcloud_storage";
    private static final String GOOGLE_DRIVE_FOLDER_APP_PROPERTY_KEY = "fragcloud_root_storage";

    /** Directory to store user credentials for this application. */
    private static final String DATA_STORE_DIR = System.getProperty("user.home") + "/.credentials/";

    /**
     * Global instance of the {@link FileDataStoreFactory}.
     */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/drive-java-quickstart
     */
    private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE_METADATA_READONLY, DriveScopes.DRIVE);

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    private Credential credential;

    private static Drive drive;
    private Object folderId;

    /**
     * Creates an authorized Credential object.
     */
    public void authorize() {
        try {
            java.io.File file = new java.io.File(DATA_STORE_DIR + getCredentials());
            DATA_STORE_FACTORY = new FileDataStoreFactory(file);

            // Load client secrets.
            InputStream in = GoogleDriveBucketEntity.class.getResourceAsStream("/public/client_secret.json");
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

            // Build flow and trigger user authorization request.
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                    HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                    .setDataStoreFactory(DATA_STORE_FACTORY)
                    .setAccessType("offline")
                    .build();
            credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
            Logging.info("Credentials saved to %s", file.getAbsolutePath());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Drive getDriveService() {
        if (drive == null) {
            authorize();
            drive = new Drive.Builder(
                    HTTP_TRANSPORT, JSON_FACTORY, credential)
                    .setApplicationName(APPLICATION_NAME)
                    .build();
        }
        return drive;
    }

    public static void main(String[] args) throws IOException {
        // Build a new authorized API client service.
        Drive service = new GoogleDriveBucketEntity().getDriveService();

        // Print the names and IDs for up to 10 files.
        FileList result = service.files().list()
                .setPageSize(10)
                .setFields("nextPageToken, files(id, name)")
                .execute();
        List<File> files = result.getFiles();
        if (files == null || files.size() == 0) {
            Logging.info("No files found.");
        } else {
            Logging.info("Files:");
            for (File file : files) {
                Logging.info("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }

    @Override
    public void listen() {
        // TODO: Listen for file changes in the cloud

//        // Print the names and IDs for up to 10 files.
//        FileList result = null;
//        try {
//            result = getDriveService().files().list()
//                    .setPageSize(10)
//                    .setFields("nextPageToken, files(id, name)")
//                    .execute();
//            List<File> files = result.getFiles();
//            if (files == null || files.size() == 0) {
//                Logging.info("No files found.");
//            } else {
//                Logging.info("Files:");
//                for (File file : files) {
//                    Logging.info("%s (%s)", file.getName(), file.getId());
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public boolean uploadFile(FileEntity file) {
        try {
            File fileMetadata = new File();
            fileMetadata.setParents(Collections.singletonList(getRootFolderId()));
            fileMetadata.setName(file.getName());
            fileMetadata.setMimeType(file.getMimeType());
            FileContent mediaContent = new FileContent(file.getMimeType(), file.getFile());
            File request = getDriveService().files().create(fileMetadata, mediaContent)
                    .setFields("id")
                    .execute();
            // TODO: store this as file.remoteId on the FileEntity
            file.setBucket(this.getId());
            file.setRemoteId(request.getId());
//            Logging.info("Drive.File ID: %s", request.getId());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public long getStorageUsed() {
        try {
            About about = getDriveService().about().get().setFields("storageQuota").execute();
            About.StorageQuota storageQuota = about.getStorageQuota();
            return storageQuota.getUsage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public long getStorageTotal() {
        try {
            About about = getDriveService().about().get().setFields("storageQuota").execute();
            About.StorageQuota storageQuota = about.getStorageQuota();
            return storageQuota.getLimit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public long getStorageRemaining() {
        try {
            About about = getDriveService().about().get().setFields("storageQuota").execute();
            About.StorageQuota storageQuota = about.getStorageQuota();
            return storageQuota.getLimit() - storageQuota.getUsage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public File getFile(String query) {
        try {
            String pageToken = null;
            FileList result = getDriveService().files().list()
                    .setQ(query)
//                    .setSpaces("drive") // TODO: Apparently there is a hidden appData folder we can store the data in if we want to!
                    .setFields("nextPageToken, files(id, name)")
                    .setPageToken(pageToken)
                    .execute();
            for (File file : result.getFiles()) {
                return file;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This returns the root folder ID for this app, not the root folder ID for the google drive account.
     * @return
     */
    public String getRootFolderId() {
        File existingFile = getFile(String.format("appProperties has { key='%s' and value='1' } and mimeType = 'application/vnd.google-apps.folder' and trashed = false", GOOGLE_DRIVE_FOLDER_APP_PROPERTY_KEY));
        if (existingFile != null) {
            return existingFile.getId();
        }

        Map<String, String> appProperties = new HashMap();
        appProperties.put(GOOGLE_DRIVE_FOLDER_APP_PROPERTY_KEY, "1");

        File createdFile = createFolder(GOOGLE_DRIVE_FOLDER_NAME, appProperties);
        return createdFile.getId();
    }

    public File createFolder(String name, Map<String, String> appProperties) {
        try {
            File fileMetadata = new File();
            fileMetadata.setName(name);
            if (appProperties != null) fileMetadata.setAppProperties(appProperties);
            fileMetadata.setMimeType("application/vnd.google-apps.folder");

            File file = getDriveService().files().create(fileMetadata)
                    .setFields("id")
                    .execute();
            Logging.info("Drive.Folder ID: %s", file.getId());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
