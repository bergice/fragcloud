package fragcloud.backend.sync.watchers;

public enum FileEvent {
    FILE_CREATED,
    FILE_DELETED,
    FILE_MODIFIED,
    FILE_RENAMED,
}
