package fragcloud.backend.sync.watchers;

import fragcloud.backend.framework.FileEntity;
import fragcloud.utils.Logging;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.*;

public class UploadVisitor extends SimpleFileVisitor<Path> {
    // Print information about
    // each type of file.
    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attr) {
        if (attr.isSymbolicLink()) {
//            processFile(path, attr);
            Logging.fine("Symbolic link: %s ", path.toFile().getAbsolutePath());
        } else if (attr.isRegularFile()) {
            Logging.fine("Regular file: %s (%d bytes)", path.toFile().getAbsolutePath(), attr.size());
            processFile(path, attr);
        } else {
            Logging.fine("Other: %s ", path.toFile().getAbsolutePath());
        }
        return CONTINUE;
    }

    public void processFile(Path path, BasicFileAttributes attr) {
        FileEntity.processForPath(path, attr);
    }

    // Print each directory visited.
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        Logging.fine("Directory: %s", dir);
        return CONTINUE;
    }

    // If there is some error accessing
    // the file, let the user know.
    // If you don't override this method
    // and an error occurs, an IOException
    // is thrown.
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        Logging.warning(exc.toString());
        return CONTINUE;
    }
}