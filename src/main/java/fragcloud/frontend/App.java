package fragcloud.frontend;

import fragcloud.backend.cloud.CloudService;
import fragcloud.backend.sync.SyncService;
import fragcloud.utils.Logging;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.control.*;
import javafx.stage.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Menu;
import java.awt.MenuItem;
import java.util.Optional;

public class App extends Application {
    private Stage rootStage;

    @Override
    public void start(Stage rootStage) throws Exception {
        this.rootStage = rootStage;
        rootStage.setOnCloseRequest(getConfirmCloseEventHandler);
        createStage(rootStage);

//        addTray();

        CloudService.getInstance().addBuckets();
        SyncService syncService = new SyncService();
        syncService.addFileWatchers();
    }

    public void createStage(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    private EventHandler<WindowEvent> getConfirmCloseEventHandler = event -> {
        System.exit(1);

        Alert closeConfirmation = new Alert(
                Alert.AlertType.CONFIRMATION,
                "Are you sure you want to exit?"
        );
        Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(
                ButtonType.OK
        );
        exitButton.setText("Exit");
        closeConfirmation.setHeaderText("Confirm Exit");
        closeConfirmation.initModality(Modality.APPLICATION_MODAL);
        closeConfirmation.initOwner(rootStage);

        // normally, you would just use the default alert positioning,
        // but for this simple sample the main stage is small,
        // so explicitly position the alert so that the main window can still be seen.
        closeConfirmation.setX(rootStage.getX());
        closeConfirmation.setY(rootStage.getY() + rootStage.getHeight());

        Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
        if (!ButtonType.OK.equals(closeResponse.get())) {
            event.consume();
        }
        else {
            // TODO: SyncService/CloudService have unmanaged threads so we force exit here to kill them, otherwise the application will continue to run!
            System.exit(1);
        }
    };

    public void addTray() {
        //Check the SystemTray is supported
        if (!SystemTray.isSupported()) {
            Logging.info("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();

        final SystemTray tray = SystemTray.getSystemTray();

        // Create a pop-up menu components
        MenuItem aboutItem = new MenuItem("About");
        CheckboxMenuItem cb1 = new CheckboxMenuItem("Set auto size");
        CheckboxMenuItem cb2 = new CheckboxMenuItem("Set tooltip");
        Menu displayMenu = new Menu("Display");
        MenuItem errorItem = new MenuItem("Error");
        MenuItem warningItem = new MenuItem("Warning");
        MenuItem infoItem = new MenuItem("Info");
        MenuItem noneItem = new MenuItem("None");
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to pop-up menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(cb1);
        popup.add(cb2);
        popup.addSeparator();
        popup.add(displayMenu);
        displayMenu.add(errorItem);
        displayMenu.add(warningItem);
        displayMenu.add(infoItem);
        displayMenu.add(noneItem);
        popup.add(exitItem);

        try {
            ImageIcon icon = new ImageIcon("image.png");
            Image image = icon.getImage();
//            File pathToFile = new File("logo.png");
//            Image image = ImageIO.read(pathToFile);
            final TrayIcon trayIcon = new TrayIcon(image);
            trayIcon.setPopupMenu(popup);
            tray.add(trayIcon);
//        } catch (IOException ex) {
//            ex.printStackTrace();
        } catch (AWTException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {
        launch(args);
    }
}
