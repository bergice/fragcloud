package fragcloud.utils;

public class Stopwatch {
    private long startMs;

    public Stopwatch(String message, Object... params)
    {
        startMs = System.currentTimeMillis();
        Logging.info(message, params);
    }

    public long end(String message, Object... params) {
        long endMs = System.currentTimeMillis();
        long timeElapsedMs = endMs - startMs;
        Logging.info("%s, completed in %d seconds", String.format(message, params), timeElapsedMs/1000);
        return timeElapsedMs;
    }
}
